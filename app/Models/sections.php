<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;
use Illuminate\Support\Facades\Auth;

class sections extends Model
{
    protected $fillable = [
        'section_name',
        'description',
        'Created_by',
    ];
}
