<?php

//use App\Http\Controllers\AdminController;
use App\Http\Controllers\InvoiceAttachmentsController;
use App\Http\Controllers\InvoicesController;
use App\Http\Controllers\ProductsController;
use App\Http\Controllers\SectionsController;
use App\Http\Controllers\InvoicesDetailsController;
use Illuminate\Support\Facades\Route;
use App\Http\Controllers\AdminController;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

//  Route::get('/', function () {
//      return view('welcome');
//  });



//Route::get('/{page}', "AdminController@index");





Auth::routes();

Route::get('/', function () {
    return view('auth.login' );
 });
Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
Route::resource('invoices', InvoicesController::class);
Route::get('/edit_invoice/{id}', [InvoicesController::class, 'edit']);
// Route::get('/destroy', [InvoicesController::class, 'destroy']);
Route::get('/InvoicesDetails/{id}', [InvoicesDetailsController::class, 'edit']);
Route::resource('InvoiceAttachments', InvoiceAttachmentsController::class);
Route::post('delete_file', [InvoicesDetailsController::class, 'destroy'])->name('delete_file');
Route::resource('sections', SectionsController::class);
//Route::post('delete_file', 'InvoicesDetailsController@destroy')->name('delete_file');
// Route::get('/section/{id}', 'InvoicesController@getproducts');
Route::get('/section/{id}', [InvoicesController::class, 'getproducts']);
Route::resource('products', ProductsController::class);
Route::get('/{page}',[AdminController::class, 'index']);

//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
//
//
//
//Route::get('/home', [App\Http\Controllers\HomeController::class, 'index'])->name('home');
